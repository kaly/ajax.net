﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ej1.aspx.vb" Inherits="Curso_ej1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Problema ej1</title>
    <script type="text/javascript">
        addEvent(window, 'load', inicializarEventos, false);

        function inicializarEventos() {
            var ob;
            for (f = 1; f <= 12; f++) {
                ob = document.getElementById('enlace' + f);
                addEvent(ob, 'click', presionEnlace, false);
            }
        }

        function presionEnlace(e) {
            if (window.event) {
                window.event.returnValue = false;
                var url = window.event.srcElement.getAttribute('href');
                cargarHoroscopo(url);
            }
            else
                if (e) {
                    e.preventDefault();
                    var url = e.target.getAttribute('href');
                    cargarHoroscopo(url);
                }
        }
        
        var conexion1;
        function cargarHoroscopo(url) {
            if (url == '') {
                return;
            }
            conexion1 = crearXMLHttpRequest();
            conexion1.onreadystatechange = procesarEventos;
            conexion1.open("GET", url, true);
            conexion1.send(null);
        }

        function procesarEventos() {
            var detalles = document.getElementById("detalles");
            if (conexion1.readyState == 4) {
                detalles.innerHTML = conexion1.responseText;
            }
            else {
                detalles.innerHTML = 'Cargando...';
            }
        }

        //***************************************
        //Funciones comunes a todos los problemas
        //***************************************
        function addEvent(elemento, nomevento, funcion, captura) {
            if (elemento.attachEvent) {
                elemento.attachEvent('on' + nomevento, funcion);
                return true;
            }
            else
                if (elemento.addEventListener) {
                    elemento.addEventListener(nomevento, funcion, captura);
                    return true;
                }
                else
                    return false;
        }

        function crearXMLHttpRequest() {
            var xmlHttp = null;
            if (window.ActiveXObject)
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            else
                if (window.XMLHttpRequest)
                    xmlHttp = new XMLHttpRequest();
            return xmlHttp;
        }
    </script>
    <style type="text/css">
        #menu
        {
            font-family: Arial;
            margin: 5px;
        }
        
        #menu p
        {
            margin: 0px;
            padding: 0px;
        }
        
        #menu a
        {
            display: block;
            padding: 3px;
            width: 160px;
            background-color: #f7f8e8;
            border-bottom: 1px solid #eee;
            text-align: center;
        }
        
        #menu a:link, #menu a:visited
        {
            color: #f00;
            text-decoration: none;
        }
        
        #menu a:hover
        {
            background-color: #369;
            color: #fff;
        }
        
        #detalles
        {
            background-color: #ffc;
            text-align: left;
            font-family: verdana;
            border-width: 0;
            padding: 5px;
            border: 1px dotted #fa0;
            margin: 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Signos del horóscopo.</h1>
        <span>http://www.ajaxya.com.ar/temarios/descripcion.php?cod=9&punto=3</span>
        <div id="menu">
            <p><a id="enlace1" href="ej1.aspx?cod=1">Aries</a></p>
            <p><a id="enlace2" href="ej1.aspx?cod=2">Tauro</a></p>
            <p><a id="enlace3" href="ej1.aspx?cod=3">Geminis</a></p>
            <p><a id="enlace4" href="ej1.aspx?cod=4">Cancer</a></p>
            <p><a id="enlace5" href="ej1.aspx?cod=5">Leo</a></p>
            <p><a id="enlace6" href="ej1.aspx?cod=6">Virgo</a></p>
            <p><a id="enlace7" href="ej1.aspx?cod=7">Libra</a></p>
            <p><a id="enlace8" href="ej1.aspx?cod=8">Escorpio</a></p>
            <p><a id="enlace9" href="ej1.aspx?cod=9">Sagitario</a></p>
            <p><a id="enlace10" href="ej1.aspx?cod=10">Capricornio</a></p>
            <p><a id="enlace11" href="ej1.aspx?cod=11">Acuario</a></p>
            <p><a id="enlace12" href="ej1.aspx?cod=12">Piscis</a></p>
        </div>
        <div id="detalles">Seleccione su signo.</div>
    </form>
</body>
</html>
