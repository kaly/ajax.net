﻿
Partial Class Curso_ej1
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.QueryString("cod") = "1" Then
            Response.Write("<strong>Aries:</strong> Hoy los cambios serán físicos, personales, de carácter, Te sentirás " +
                "impulsivo y tomarás  iniciativas. Período en donde considerarás unirte a agrupaciones de " +
                "beneficencia, o de ayuda a los demás.")
        ElseIf Request.QueryString("cod") = "2" Then
            Response.Write("<strong>Tauro:</strong> Hoy los cambios serán privados, íntimos. Recuerdos. Ayuda, " +
                "solidaridad. Asuntos en lugares de retiro. Tu cónyuge puede aportar buen status a tu vida o apoyo a " +
                "tu profesión.")
        ElseIf Request.QueryString("cod") = "3" Then
            Response.Write("<strong>Géminis:</strong> Los asuntos de hoy tienen que ver con las amistades, reuniones, " +
                "actividades con ellos. Día esperanzado, ilusiones. Mucha energía sexual y fuerza emocional. Deseos difíciles de controlar.")
        ElseIf Request.QueryString("cod") = "4" Then
            Response.Write("<strong>Cancer:</strong> Este día la profesión y las relaciones con superiores y con tu " +
                "madre serán de importancia. Actividad en relación a estos temas. Momentos positivos con compañeros " +
                "de trabajo. Actividad laboral agradable.")
        ElseIf Request.QueryString("cod") = "5" Then
            Response.Write("<strong>Leo:</strong> Este día los estudios, los viajes, el extranjero y la espiritualidad " +
                "serán lo importante. Pensamientos, religión y filosofía también. Vivencias kármicas de la época te " +
                "vuelven responsable tomando decisiones.")
        ElseIf Request.QueryString("cod") = "6" Then
            Response.Write("<strong>Virgo:</strong> Para este día toma importancia tu vida sexual, tal vez miedos, temas " +
                "legales, juicios o herencias. Experiencias extrañas. Hay karma de prueba durante este período en tu " +
                "parte psicológica, generándose algunos replanteos.")
        ElseIf Request.QueryString("cod") = "7" Then
            Response.Write("<strong>Libra:</strong> Hoy todo asunto tiene que ver con tu pareja, también con socios, con " +
                "la gente o el público. Ellos serán lo más importante del día. Ganancias a través de especulaciones o " +
                "del juego. Actividades vocacionales artísticas.")
        ElseIf Request.QueryString("cod") = "8" Then
            Response.Write("<strong>Escorpio:</strong> Hoy todo asunto tiene que ver con temas de trabajo y de salud. " +
                "Presta atención a ambos. Experiencias diversas con compañeros. Durante este período tendrás muchos " +
                "recursos para ganar dinero.")
        ElseIf Request.QueryString("cod") = "9" Then
            Response.Write("<strong>Sagitario:</strong> Durante este día se vivirán cambios en relación a los noviazgos " +
                "o a los hijos. Creatividad, actividad, diversiones y salidas. Período de encuentros con personas o " +
                "situaciones que te impresionan.")
        ElseIf Request.QueryString("cod") = "10" Then
            Response.Write("<strong>Capricornio:</strong> Los cambios del día tienen que ver con tu hogar, con la " +
                "convivencia y con el padre. Asuntos relativos al carácter en la convivencia. El karma de " +
                "responsabilidad de estos momentos te acercará al mundo de lo desconocido, mucha madurez y contacto con el más allá.")
        ElseIf Request.QueryString("cod") = "11" Then
            Response.Write("<strong>Acuario:</strong> Hoy todo asunto tiene que ver con el entorno inmediato, hermanos y " +
                "vecinos, con la comunicación, los viajes cortos o traslados frecuentes. El hablar y trasladarse será " +
                "importante hoy. Mentalidad e ideas activas.")
        ElseIf Request.QueryString("cod") = "12" Then
            Response.Write("<strong>Piscis:</strong> Durante este día se vivirán cambios en la economía, movimientos en " +
                "los ingresos, negocios, valores. Momentos de gran fuerza y decisión profesionales, buscarás el liderazgo.")
        End If
    End Sub
End Class
