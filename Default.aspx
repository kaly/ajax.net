﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul>
            <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Curso/ej1.aspx">ej 1. Ejemplo de Ajax</asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Curso/ej2.aspx">ej 2. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Curso/ej3.aspx">ej 3. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Curso/ej4.aspx">ej 4. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Curso/ej5.aspx">ej 5. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Curso/ej6.aspx">ej 6. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Curso/ej7.aspx">ej 7. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Curso/ej8.aspx">ej 8. </asp:HyperLink></li>
            <li><asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/Curso/ej9.aspx">ej 9. </asp:HyperLink></li>
        </ul>
    </div>
    </form>
</body>
</html>
