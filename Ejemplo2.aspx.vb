﻿
Imports System.Data
Imports System.Web.Services
Imports System.Configuration
Imports System.Data.SqlClient

Partial Class Ejemplo2
    Inherits System.Web.UI.Page

    Private Shared PageSize As Integer = 10


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDummyRow()
        End If
    End Sub

    Private Sub BindDummyRow()
        Dim dummy As New DataTable()
        dummy.Columns.Add("IdArticulo")
        dummy.Columns.Add("NombreArticulo")
        dummy.Columns.Add("DescripcionArticulo")
        dummy.Rows.Add()
        gvArticulos.DataSource = dummy
        gvArticulos.DataBind()
    End Sub

    <WebMethod()> _
    Public Shared Function GetCustomers(pageIndex As Integer) As String
        Dim query As String = "[GetArticulo_Pager]"
        Dim cmd As New SqlCommand(query)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.AddWithValue("@PageIndex", pageIndex)
        cmd.Parameters.AddWithValue("@PageSize", PageSize)
        cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output
        Return GetData(cmd, pageIndex).GetXml()

        '   ' Esto permite crear un archivo con el esquema xml
        'Dim str As String = GetData(cmd, pageIndex).GetXml()
        'Dim xd As New ConfigXmlDocument()
        'xd.LoadXml(str)
        'xd.Save(HttpContext.Current.Request.MapPath("~/borrar/archivo3.xml"))
        'Return str
    End Function

    ' ''' <summary>
    ' ''' UTILIZANDO UN PROCEDIMIENTO ALMACENADO DE SQL
    ' ''' </summary>
    ' ''' <param name="cmd"></param>
    ' ''' <param name="pageIndex"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Shared Function GetData(cmd As SqlCommand, pageIndex As Integer) As DataSet
    '    Dim strConnString As String = ConfigurationManager.ConnectionStrings("Web_ConnectionString").ConnectionString
    '    Using con As New SqlConnection(strConnString)
    '        Using sda As New SqlDataAdapter()
    '            cmd.Connection = con
    '            sda.SelectCommand = cmd
    '            Using ds As New DataSet()
    '                sda.Fill(ds, "A_Articulo")
    '                Dim dt As New DataTable("Pager")
    '                dt.Columns.Add("PageIndex")
    '                dt.Columns.Add("PageSize")
    '                dt.Columns.Add("RecordCount")
    '                dt.Rows.Add()
    '                dt.Rows(0)("PageIndex") = pageIndex
    '                dt.Rows(0)("PageSize") = PageSize
    '                dt.Rows(0)("RecordCount") = cmd.Parameters("@RecordCount").Value
    '                ds.Tables.Add(dt)
    '                Return ds
    '            End Using
    '        End Using
    '    End Using
    'End Function

    '<WebMethod()> _
    'Public Shared Function GetCustomers(pageIndex As Integer) As String
    '    Dim query As String = "[GetCustomers_Pager]"
    '    Dim cmd As New SqlCommand(query)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.AddWithValue("@PageIndex", pageIndex)
    '    cmd.Parameters.AddWithValue("@PageSize", PageSize)
    '    cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4).Direction = ParameterDirection.Output
    '    Return GetData(cmd, pageIndex).GetXml()
    'End Function

    Private Shared Function GetData(cmd As SqlCommand, pageIndex As Integer) As DataSet
        Using ds As New DataSet()
            ' Obtener el número de registros
            Dim recordCount As Integer = DAL.ObtenerNumeroFilasTabla("A_Articulo")
            ' Select que permite la paginación
            Dim sql As String =
                "DECLARE @PageIndex INT = " + pageIndex.ToString + ", @PageSize INT = " + PageSize.ToString + ", @RecordCount INT = " + recordCount.ToString + "; " +
                "SET NOCOUNT ON; " +
                "SELECT ROW_NUMBER() OVER " +
                "(ORDER BY [IdArticulo] ASC)AS RowNumber, " +
                "[IdArticulo], [NombreArticulo], [DescripcionArticulo] " +
                "INTO #Results FROM [A_Articulo] " +
                " SELECT @RecordCount = COUNT(*) FROM #Results " +
                "SELECT * FROM #Results " +
                "WHERE RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1 " +
                "DROP TABLE #Results "
            Dim dt As DataTable = DAL.LeerTabla(sql)
            dt.TableName = "A_Articulo"
            ds.Tables.Add(dt)

            ' Tabla para el DataPager
            Dim dtPager As New DataTable("Pager")
            dtPager.Columns.Add("PageIndex")
            dtPager.Columns.Add("PageSize")
            dtPager.Columns.Add("RecordCount")
            dtPager.Rows.Add()
            dtPager.Rows(0)("PageIndex") = pageIndex
            dtPager.Rows(0)("PageSize") = PageSize
            dtPager.Rows(0)("RecordCount") = "202"
            ds.Tables.Add(dtPager)

            Return ds
        End Using
    End Function


End Class
