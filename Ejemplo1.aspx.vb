﻿
Partial Class Ejemplo1
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCurrentTime(ByVal name As String) As String
        Return "Hola " & name & Environment.NewLine & " La hoa asctual es: " & DateTime.Now.ToString()
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function SendParameters(nombre As String, edad As Integer) As String
        Return String.Format("Nombre: {0}{2}Edad: {1}", nombre, edad, Environment.NewLine)
    End Function

End Class
