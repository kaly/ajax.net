﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ejemplo1.aspx.vb" Inherits="Ejemplo1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script src="scripts/jquery-1.7.1.min.js" type="text/javascript"></script>

    <script type = "text/javascript">
        function ShowCurrentTime() {
            $.ajax({
                type: "POST",
                url: "Ejemplo1.aspx/GetCurrentTime",
                data: '{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
        }
    </script>


    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/json2/20130526/json2.min.js"></script>
    <script type="text/javascript">
        $(function () {
           /* $("#btnSubmit").click(function () { */
            $("[id*=btnSubmit]").click(function () {
                var obj = {};
                obj.nombre = $.trim($("[id*=txtNombre]").val());
                obj.edad = $.trim($("[id*=txtEdad]").val());
                $.ajax({
                    type: "POST",
                    url: "Ejemplo1.aspx/SendParameters",
                    data: JSON.stringify(obj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        alert(r.d);
                    }
                });
                return false;
            });
        });
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>
            Ejemplo 1. utilización AJAX sencilla</h1>
        <h4>
            El input de html ejecuta una función de javascript que a su vez llama a la función
            de servidor(vb.net) GetCurrentTime para obtener los datos a mostrar.</h4>
        <h4>
            http://aspsnippets.com/Articles/Call-ASPNet-Page-Method-using-jQuery-AJAX-Example.aspx
        </h4>
    </div>
    <div>
        Nombre:
        <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
        <input id="btnGetTime" type="button" value="Ver la hora actual" onclick="ShowCurrentTime()" />
    </div>
    <br />
    <br />
    <br />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                Nombre:
            </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server" Text="Luis" />
            </td>
        </tr>
        <tr>
            <td>
                Edad:
            </td>
            <td>
                <asp:TextBox ID="txtEdad" runat="server" Text="39" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" Text="Enviar" runat="server" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
