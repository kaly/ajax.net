﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Web.Caching
Imports System.ComponentModel

Public Class DAL
    Shared _DataMethod As New DataMethod

    Private Shared Function Comprobaciones(sql As String) As String
        ' Función para comprobar los datos y sentencias sql recibidos y evitar inyecciones de código no deseados.
        Dim isOK As Boolean = True

        ' Quitamos los espacios del principio y final.
        sql.Trim()

        'Comprobamos que no hay palabras reservadas de SQL. 
        Dim palabrasReservadas As New ArrayList
        'palabrasReservadas.Add("*")
        palabrasReservadas.Add("HAVING")
        palabrasReservadas.Add("DELETE")
        palabrasReservadas.Add("INSERT")
        palabrasReservadas.Add("UPDATE")
        'palabrasReservadas.Add("AND")
        palabrasReservadas.Add("IN")
        palabrasReservadas.Add("VALUES")
        palabrasReservadas.Add("CREATE")
        palabrasReservadas.Add("DROP")
        palabrasReservadas.Add("ALTER")
        palabrasReservadas.Add("GRANT")
        palabrasReservadas.Add("REVOKE")
        palabrasReservadas.Add("COMMIT")
        palabrasReservadas.Add("ROLLBACK")
        palabrasReservadas.Add("DECLARE")
        palabrasReservadas.Add("OPEN")
        palabrasReservadas.Add("FETCH")
        palabrasReservadas.Add("CLOSE")

        For Each p As String In palabrasReservadas
            Dim sq() As String = Split(sql)
            For Each s In sq
                If s.ToUpper = p.ToUpper Then isOK = False : Return "Comprobaciones - Excepción de seguridad: no se ha procesado la solicitud: \n" + sql
            Next
            '    If sql.ToUpper().Contains(p) Then isOK = False : Return "Comprobaciones - Excepción de seguridad: no se ha procesado la solicitud: \n" + sql
        Next

        Return ""
    End Function

    'Leer tabla sin utilizar el Cache
    Public Shared Function LeerTabla(sql As String) As DataTable
        Try
            Dim comando As SqlCommand = _DataMethod.CrearComando
            comando.CommandText = sql
            Return _DataMethod.EjecutarComandoSelect(comando)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Leer tabla utilizando la Cache
    Public Shared Function LeerTabla(NombreCache As String, sql As String, ByVal miCache As Cache) As DataTable
        Try
            'Dim miCache As New Cache()
            Dim dt As DataTable
            dt = miCache(NombreCache)
            If dt Is Nothing Then
                Dim comando As SqlCommand = _DataMethod.CrearComando
                comando.CommandText = sql
                dt = _DataMethod.EjecutarComandoSelect(comando)
                miCache.Insert(NombreCache, dt, Nothing, DateTime.Now.AddMinutes(30), TimeSpan.Zero)
            End If
            Return dt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' Leer tabla decodificando los campos para mostrar correctamente el HTML
    Public Shared Function LeerTablaDecode(sql As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim comando As SqlCommand = _DataMethod.CrearComando()
            comando.CommandText = sql
            dt = _DataMethod.EjecutarComandoSelect(comando)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    For x As Integer = 0 To dr.ItemArray.Length - 1
                        If Not IsDBNull(dr.Item(x)) Then
                            Dim t As Type = dr.Item(x).GetType

                            If t.Name = "Guid" Then
                                Dim id As String = TypeDescriptor.GetConverter(dr.Item(x)).ConvertTo(dr.Item(x), GetType(String))
                                dr.Item(x) = HttpContext.Current.Server.HtmlDecode(id)
                            ElseIf t.Name = "TimeSpan" Then

                            Else
                                dr.Item(x) = HttpContext.Current.Server.HtmlDecode(dr.Item(x))
                            End If
                        End If
                    Next
                Next
            End If

            Return dt
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Obtener el numero de filas de la tabla
    Public Shared Function ObtenerNumeroFilasTabla(tabla As String, Optional condicion As String = "") As Integer
        Try
            Dim comando As SqlCommand = _DataMethod.CrearComando
            If condicion <> "" Then condicion = "WHERE " + condicion
            comando.CommandText = "SELECT COUNT(*) as num FROM " + tabla + " " + condicion
            Dim dt As DataTable = _DataMethod.EjecutarComandoSelect(comando)
            If Not IsNothing(dt) Then
                Return dt.Rows(0).Item("Num").ToString
            Else
                Throw New Exception("Datos no encontrados en la base de datos. Tabla " + tabla + ", condición " + condicion)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCIÓN GENÉRICA PARA INSERTAR DATOS
    ''' </summary>
    ''' <param name="tabla"></param>
    ''' <param name="campo"></param>
    ''' <param name="valor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertarTabla(tabla As String, campo() As String, valor() As String, Optional comprobar As Boolean = True) As Boolean
        Try
            ' Compruebo que el nº de campos y de valores es el mismo
            If campo.Count <> valor.Count Then Return "Error en " + tabla + ": Número de campos y de valores es distinto. No se ha procesado la inserción de datos."

            ' Comprobación de texto introducido.
            Dim isOk As String

            If comprobar = True Then
                For Each v As String In valor
                    isOk = Comprobaciones(v)
                    If isOk <> "" Then Return isOk
                Next
            End If


            ' preparo la sentencia de inserción de datos
            Dim sql As New StringBuilder
            Dim sqlCampos As New StringBuilder
            Dim sqlPArametros As New StringBuilder

            For x As Integer = 0 To campo.Count - 1
                If x > 0 Then
                    sqlCampos.Append(", ")
                    sqlPArametros.Append(", ")
                End If
                sqlCampos.Append(campo(x).ToString)
                sqlPArametros.Append("@" + campo(x).ToString)
            Next
            sql.Append("set language 'español'; INSERT INTO " + tabla.ToString + " (" + sqlCampos.ToString & ") VALUES (" + sqlPArametros.ToString + ")")

            ' creo comando
            Dim comando As SqlCommand = _DataMethod.CrearComando()
            comando.CommandText = sql.ToString

            ' Relleno los parametros
            For x As Integer = 0 To campo.Count - 1
                comando.Parameters.AddWithValue("@" + campo(x).ToString, valor(x).ToString)
            Next

            'ejecuto el comando
            Return _DataMethod.EjecutarComandoInsert(comando)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCIÓN GENÉRICA PARA INSERTAR DATOS
    ''' </summary>
    ''' <param name="tabla"></param>
    ''' <param name="campo_igual_valor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsertarTabla(tabla As String, comprobar As Boolean, ParamArray campo_igual_valor() As String) As Boolean
        Try
            ' var
            Dim campos() As String = campo_igual_valor
            Dim isOk As String

            ' Comprobación de texto introducido
            If comprobar = True Then
                For Each c As String In campos
                    isOk = Comprobaciones(c)
                    If isOk <> "" Then Return False
                Next
            End If


            ' preparo la sentencia de inserción de datos
            Dim sql As New StringBuilder
            Dim sqlCampos As New StringBuilder
            Dim sqlPArametros As New StringBuilder

            For x As Integer = 0 To campos.Count - 1
                If x > 0 Then
                    sqlCampos.Append(", ")
                    sqlPArametros.Append(", ")
                End If
                Dim _Split() As String = Split(campos(x), "=")
                sqlCampos.Append(_Split(0).ToString)
                sqlPArametros.Append("@" + _Split(0).ToString)
            Next
            sql.Append("set language 'español'; INSERT INTO " + tabla.ToString + " (" + sqlCampos.ToString & ") VALUES (" + sqlPArametros.ToString + ")")

            ' creo comando
            Dim comando As SqlCommand = _DataMethod.CrearComando()
            comando.CommandText = sql.ToString

            ' Relleno los parametros
            For x As Integer = 0 To campos.Count - 1
                Dim _Split() As String = Split(campos(x), "=")
                comando.Parameters.AddWithValue("@" + _Split(0).ToString.TrimEnd(), _Split(1).ToString.TrimStart)
            Next

            'ejecuto el comando
            Return _DataMethod.EjecutarComandoInsert(comando)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCIÓN GENÉRICA PARA ACTUALIZAR DATOS DE UN REGISTRO
    ''' </summary>
    ''' <param name="tabla"></param>
    ''' <param name="condicion"></param>
    ''' <param name="campo"></param>
    ''' <param name="valor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ActualizarTabla(tabla As String, condicion As String, campo() As String, valor() As String) As Boolean
        Try
            ' Compruebo que el nº de campos y de valores es el mismo
            If campo.Count <> valor.Count Then Return "Error en " + tabla + ": Numero de campos y de valores es distinto. No se ha procesado la actualización de datos."

            ' Comprobación de texto introducido.
            Dim isOk As String
            For Each v As String In valor
                isOk = Comprobaciones(v)
                If isOk <> "" Then Return isOk
            Next

            ' var sql
            Dim sql As New StringBuilder

            sql.Append("set language 'español'; UPDATE " + tabla + " SET ")

            ' preparo la sentencia de inserción de datos
            For x As Integer = 0 To campo.Count - 1
                If x > 0 Then sql.Append(", ")
                sql.Append(campo(x).ToString)
                sql.Append(" = '")
                sql.Append(valor(x).ToString)
                sql.Append("'")
            Next
            sql.Append(" WHERE " + condicion)


            Dim comando As SqlCommand = _DataMethod.CrearComando
            comando.CommandText = sql.ToString
            Return _DataMethod.EjecutarComandoUpdate(comando)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' FUNCIÓN GENÉRICA PARA ACTUALIZAR DATOS DE UN REGISTRO
    ''' </summary>
    ''' <param name="tabla"></param>
    ''' <param name="condicion"></param>
    ''' <param name="campo_igual_valor"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ActualizarTabla(tabla As String, comprobar As Boolean, condicion As String, ParamArray campo_igual_valor() As String) As Boolean
        Try
            ' var
            Dim campos() As String = campo_igual_valor
            Dim isOk As String

            ' Comprobación del texto introducido.
            If comprobar = True Then
                isOk = Comprobaciones(condicion)    ' Compruebo la condición
                If isOk <> "" Then Throw New System.Exception(isOk)
                For Each c As String In campos
                    isOk = Comprobaciones(c)        ' Compruebo los campos
                    If isOk <> "" Then Throw New System.Exception(isOk)
                Next
            End If


            ' var sql
            Dim sql As New StringBuilder

            sql.Append("set language 'español'; UPDATE " + tabla + " SET ")

            ' preparo la sentencia de inserción de datos
            For x As Integer = 0 To campos.Count - 1
                If x > 0 Then sql.Append(", ")
                sql.Append(campos(x).ToString)
            Next

            ' Añado las condiciones
            sql.Append(" WHERE " + condicion)

            Dim comando As SqlCommand = _DataMethod.CrearComando
            comando.CommandText = sql.ToString
            Return _DataMethod.EjecutarComandoUpdate(comando)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' Función para borrar de una tabla.
    Public Shared Function EliminarTabla(tabla As String, condicion As String) As Boolean
        Try
            ' Comprobación de texto introducido.
            Dim isOk As String
            isOk = Comprobaciones(condicion)
            If isOk <> "" Then Return isOk

            ' var sql
            Dim sql As New StringBuilder
            Dim comando As SqlCommand = _DataMethod.CrearComando()

            sql.Append("set language 'español'; DELETE FROM " + tabla)
            sql.Append(" WHERE " + condicion)

            comando.CommandText = sql.ToString

            Return _DataMethod.EjecutarComandoDelete(comando)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
