﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class DataMethod
    Dim _connectionString As String = ConfigurationManager.ConnectionStrings("Web_ConnectionString").ConnectionString

    ''' <summary>
    ''' CREA UN COMANDO SQL STANDARD. ej un Select, el cual sera regresado por su metoro return
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CrearComando() As SqlCommand
        'Dim cadenaConexion As String = 
        Dim conexion As New SqlConnection
        Dim comando As New SqlCommand

        conexion.ConnectionString = _connectionString
        comando = conexion.CreateCommand
        comando.CommandType = CommandType.Text

        Return comando
    End Function

    ''' <summary>
    ''' CREA UN COMANDO SQL QUE PUEDA EJECUTAR UN PROCEDIMIENTO ALMACENADO (SP). 
    ''' </summary>
    ''' <param name="NombreSP"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CrearComandoSP(ByVal NombreSP As String) As SqlCommand
        Dim cadenaConexion As String = _connectionString
        Dim conexion As SqlConnection = New SqlConnection(cadenaConexion)
        Dim comando As SqlCommand = New SqlCommand(NombreSP, conexion)

        comando.CommandType = CommandType.StoredProcedure

        Return comando
    End Function

    ''' <summary>
    ''' EJECUTA UN SELECT Y NOS DEVUELVE UN DATABASE
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecutarComandoSelect(ByVal comando As SqlCommand) As DataTable
        Dim tabla As DataTable = New DataTable
        Try
            Dim adaptador As SqlDataAdapter = New SqlDataAdapter

            comando.Connection.Open()
            adaptador.SelectCommand = comando
            adaptador.Fill(tabla)
        Catch ex As Exception
            Throw ex
        Finally
            comando.Connection.Close()
            comando.Connection.Dispose()
        End Try
        Return tabla
    End Function

    ''' <summary>
    ''' EJECUTA EL COMANDO INSERT
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecutarComandoInsert(ByVal comando As SqlCommand) As Boolean
        Try
            Dim err As Boolean = False

            comando.Connection.Open()
            err = comando.ExecuteNonQuery

            '   If err = "1" Then err = ""
            If IsNumeric(err) Then err = True
            Return err

        Catch ex As Exception
            Throw ex
        Finally
            comando.Connection.Close()
            comando.Connection.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' EJECUTA EL COMANDO UPDATE
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecutarComandoUpdate(ByVal comando As SqlCommand) As Boolean
        Try
            Dim err As Boolean = False
            comando.Connection.Open()
            err = comando.ExecuteNonQuery

            If IsNumeric(err) Then err = True
            Return err

        Catch ex As Exception
            Throw ex
        Finally
            comando.Connection.Close()
            comando.Connection.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' EJECUTA EL COMANDO DELETE
    ''' </summary>
    ''' <param name="comando"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function EjecutarComandoDelete(ByVal comando As SqlCommand) As Boolean
        Try
            Dim err As Boolean = False
            comando.Connection.Open()
            err = comando.ExecuteNonQuery

            If IsNumeric(err) Then err = True
            Return err

        Catch ex As Exception
            Throw ex
        Finally
            comando.Connection.Close()
            comando.Connection.Dispose()
        End Try

    End Function

End Class
