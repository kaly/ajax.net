﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Ejemplo2.aspx.vb" Inherits="Ejemplo2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="scripts/jquery-1.7.1.min.js" type="text/javascript"></script>
    <script src="scripts/ASPSnippets_Pager.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        GetCustomers(1);
    });
    $(".Pager .page").live("click", function () {
        GetCustomers(parseInt($(this).attr('page')));
    });
    function GetCustomers(pageIndex) {
        $.ajax({
            type: "POST",
            url: "Ejemplo2.aspx/GetCustomers",
            data: '{pageIndex: ' + pageIndex + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.d);
            }
        });
    }

    function OnSuccess(response) {
        var xmlDoc = $.parseXML(response.d);
        var xml = $(xmlDoc);
        var A_Articulo = xml.find("A_Articulo");
        var row = $("[id*=gvArticulos] tr:last-child").clone(true);
        $("[id*=gvArticulos] tr").not($("[id*=gvArticulos] tr:first-child")).remove();
        $.each(A_Articulo, function () {
            var customer = $(this);
            $("td", row).eq(0).html($(this).find("IdArticulo").text());
            $("td", row).eq(1).html($(this).find("NombreArticulo").text());
            $("td", row).eq(2).html($(this).find("DescripcionArticulo").text());
            $("[id*=gvArticulos]").append(row);
            row = $("[id*=gvArticulos] tr:last-child").clone(true);
        });
        var pager = xml.find("Pager");
        $(".Pager").ASPSnippets_Pager({
            ActiveCssClass: "current",
            PagerCssClass: "pager",
            PageIndex: parseInt(pager.find("PageIndex").text()),
            PageSize: parseInt(pager.find("PageSize").text()),
            RecordCount: parseInt(pager.find("RecordCount").text())
        });
    };
</script>
<style type="text/css">
    body
    {
        font-family: Arial;
        font-size: 10pt;
    }
    .Pager span
    {
        text-align: center;
        color: #999;
        display: inline-block;
        width: 20px;
        background-color: #A1DCF2;
        margin-right: 3px;
        line-height: 150%;
        border: 1px solid #3AC0F2;
    }
    .Pager a
    {
        text-align: center;
        display: inline-block;
        width: 20px;
        background-color: #3AC0F2;
        color: #fff;
        border: 1px solid #3AC0F2;
        margin-right: 3px;
        line-height: 150%;
        text-decoration: none;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>
            Ejemplo 2. Cargar Tabla con AJAX</h1>
        <h4>
            Cargar Tabla(GridView) con AJAX y que continue cargando datos según vamos bajando
        </h4>
        <h4>
            http://aspsnippets.com/Articles/Paging-in-ASPNet-GridView-using-jQuery-AJAX.aspx
        </h4>
    </div>
    <div>
        <asp:GridView ID="gvArticulos" runat="server" AutoGenerateColumns="false" RowStyle-BackColor="#A1DCF2"
            HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" Width="100%">
            <Columns>
                <asp:BoundField ItemStyle-Width="150px" DataField="IdArticulo" HeaderText="IdArticulo" />
                <asp:BoundField ItemStyle-Width="150px" DataField="NombreArticulo" HeaderText="Nombre" />
                <asp:BoundField ItemStyle-Width="150px" DataField="DescripcionArticulo" HeaderText="Descripción" />
            </Columns>
        </asp:GridView>
        <br />
        <div class="Pager">
        </div>
    </div>
    </form>
</body>
</html>
